
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import util.arrays;
import util.files;
import util.images;
import util.images.Gradient;
import util.images.HOG;
import util.images.strel;
import util.math.matrices;

/**
 *
 * @author Mike
 */
public class Main {

    private static final String ROOT = "../../";
    private static final String res = ROOT + "res/";

    public static void main(String[] args) throws Exception {
        // testSloped();
        // testEdge();
        // testSplit();
        // testEdgeSplit();
        // testGradientSave();
        // testGradientHistogram();
        // testSubset();
        // testGradientSubset();
        // testGradientImageParse();
        // testSlopeLinePixel();
        // testMap();
        // testHOG();
        // testResize();
        testDetectionWindow();
    }

    public static void testDetectionWindow() throws IOException {
        byte[][][] bldgImage = images.loadImageBytes(res + "bldg.bmp");
        List<byte[][][]> subsets = images.detectionWindow(bldgImage, new int[]{bldgImage.length / 2, bldgImage.length}, 2, 2);
        for (int i = 0; i < subsets.size(); i++) {
            images.saveImageBytes(subsets.get(i), res + "subsets/" + i + ".bmp");
        }
    }

    public static void testResizePreserveGradients() throws IOException {
        byte[][][] inputImage = images.loadImageBytes(res + "bldg.bmp");
        int[] newRes = new int[]{100, 77};
        byte[][][] resized = images.resizeNearest(inputImage, newRes);
        HOG hogResized = images.hogRepresentation(resized);

        Gradient[][] gradients = images.findGradientsSplit(inputImage);
        Gradient[][] gradResized = images.resizeNearestG(gradients, newRes);
        HOG resizedHOG = images.gradientsToHOG(gradResized);

        images.saveImageFromIntArray(hogResized.negative_representation, res + "bldg/hogResized.bmp", BufferedImage.TYPE_INT_RGB);
        images.saveImageFromIntArray(resizedHOG.negative_representation, res + "bldg/resizedHOG.bmp", BufferedImage.TYPE_INT_RGB);

    }

    public static void testResize() throws IOException {
        byte[][][] redRect = images.solidRect(10, 10, new byte[]{(byte) 255, (byte) 1, (byte) 1});
        byte[][][] resized = images.resizeNearest(redRect, new int[]{20, 40});
        images.saveImageBytes(resized, res + "resized.bmp");

        byte[][][] smallBldg = images.loadImageBytes(res + "bldgSmall.bmp");
        byte[][][] smallBldgResized = images.resizeNearest(smallBldg, new int[]{smallBldg[0].length * 2, smallBldg.length * 2});
        images.saveImageBytes(smallBldgResized, res + "bldgSmallResized.bmp");

        byte[][][] bldg = images.loadImageBytes(res + "bldg.bmp");
        byte[][][] bldgResized = images.resizeNearest(bldg, new int[]{100, 77});
        images.saveImageBytes(bldgResized, res + "bldgResizedDown.bmp");

    }

    public static void testHOG() throws IOException {
//        HOG h = images.hogRepresentation(res + "bldg.bmp");
//        images.saveImageFromIntArray(h.negative_representation, res + "neg.bmp", BufferedImage.TYPE_INT_RGB);
//        images.saveImageFromIntArray(h.positive_representation, res + "pos.bmp", BufferedImage.TYPE_INT_RGB);
//        Map<String, List<String>> fileNameMap = files.getFileExtensionNameMap(res + "HOG");
//        System.out.println(fileNameMap);
        images.generateHOG(res + "HOG/person_analysis/", "persons");
    }

    public static void testMap() {
        Map<String, List<String>> fileNameMap = files.getFileExtensionNameMap(res);
        System.out.println(fileNameMap);
        String[] bmpFilesName = files.getNames(res, "bmp");
        System.out.println(Arrays.toString(bmpFilesName));
        Map<String, List<String>> fileLocMap = files.getFileExtensionLocationMap(res);
        System.out.println(fileLocMap);
        String[] bmpFilesLoc = files.getLocations(res, "bmp");
        System.out.println(Arrays.toString(bmpFilesLoc));
        Map<String, List<String>> personMap = files.getFileExtensionNameMap(res + "HOG/persons/");
        System.out.println(personMap);

    }

    public static void testSlopeLinePixel() throws IOException {
        byte[][][] sloped = images.slopeLinePixel(1.0, 7, new byte[]{(byte) 109, (byte) 1, (byte) 255}, false);
        images.saveImageBytes(sloped, res + "slopeTest.bmp");

        byte[][][] image_a = images.slopeLinePixel(1.0, 7, new byte[]{(byte) 100, (byte) 100, (byte) 100}, false);
        byte[][][] image_b = images.slopeLinePixel(-1.0, 7, new byte[]{(byte) 50, (byte) 50, (byte) 50}, false);
        byte[][][] image_c = images.add(new byte[][][][]{image_a, image_b});
        images.saveImageBytes(image_a, res + "a.bmp");
        images.saveImageBytes(image_b, res + "b.bmp");
        images.saveImageBytes(image_c, res + "c.bmp");

        double theta = Math.atan(1.0);
        double slope = Math.tan(theta);
        System.out.println("theta:" + theta);
        System.out.println("slope:" + slope);

        byte[][][][] h_images = images.histogramToImages(new int[]{10, 0, 2, 0, 200, 0, 2, 0, 2});
        byte[][][] combines = images.add(h_images);
        images.saveImageBytes(combines, res + "combined.bmp");

    }

    public static void testGradientImageParse() throws IOException {
        String name = "bldg";
        byte[][][] original = images.loadImageBytes(res + name + ".bmp");
        Gradient[][] gradients = images.findGradientsSplit(original);
        double[][] thetas = new double[gradients.length][gradients[0].length];
        double[][] degrees = new double[gradients.length][gradients[0].length];
        for (int ROW = 0; ROW < gradients.length; ROW++) {
            for (int COL = 0; COL < gradients[0].length; COL++) {
                thetas[ROW][COL] = gradients[ROW][COL].positiveTheta();
                degrees[ROW][COL] = gradients[ROW][COL].positiveDegree();
            }
        }
        for (int ROW = 0; ROW < gradients.length; ROW++) {
            // System.out.println(Arrays.toString(degrees[ROW]));
        }
        class Cell {

            final int[] histogram;

            public Cell(int[] histogram) {
                this.histogram = histogram;
            }

            @Override
            public String toString() {
                return "Cell{" + "histogram=" + Arrays.toString(histogram) + '}';
            }

        }

        class Block {

            final Cell[][] cells;

            public Block(Cell[][] cells) {
                this.cells = cells;
            }

            public int[] histogramVector() {
                int[] retArray = new int[cells.length * cells[0].length * cells[0][0].histogram.length];
                int index = 0;
                for (int ROW = 0; ROW < cells.length; ROW++) {
                    for (int COL = 0; COL < cells[0].length; COL++) {
                        for (int HIST = 0; HIST < cells[0][0].histogram.length; HIST++) {
                            retArray[index] = cells[ROW][COL].histogram[HIST];
                            index++;
                        }
                    }
                }
                return retArray;
            }

            @Override
            public String toString() {
                return "Block{" + "histVector=" + Arrays.toString(this.histogramVector()) + '}';
            }

        }

        int cell_size = 8;
        Cell[][] cells = new Cell[gradients.length / cell_size][gradients[0].length / cell_size];
        for (int CELL_ROW = 0; CELL_ROW < cells.length; CELL_ROW++) {
            for (int CELL_COL = 0; CELL_COL < cells[0].length; CELL_COL++) {
                Gradient[][] sub = arrays.subset(gradients, CELL_ROW * cell_size, CELL_COL * cell_size, cell_size, cell_size, new Gradient(-1, -1, -1, -1));
                int[] histogram = images.applyHistogram(sub);
                cells[CELL_ROW][CELL_COL] = new Cell(histogram);
            }
        }
        for (int ROW = 0; ROW < cells.length; ROW++) {
            for (int COL = 0; COL < cells[0].length; COL++) {
                System.out.println(cells[ROW][COL]);
            }
        }
        int block_size = 2;
        Block[][] blocks = new Block[cells.length - block_size + 1][cells[0].length - block_size + 1];
        for (int ROW = 0; ROW < blocks.length; ROW++) {
            for (int COL = 0; COL < blocks[0].length; COL++) {
                Cell[][] sub = arrays.subset(cells, ROW, COL, block_size, block_size, new Cell(null));
                blocks[ROW][COL] = new Block(sub);
                System.out.println(blocks[ROW][COL].histogramVector().length);
            }
        }

        int dim = 9;
        strel square = strel.square(dim);
        int[][] retImagePOS = new int[cells.length * dim][cells[0].length * dim];
        int[][] retImageNEG = new int[cells.length * dim][cells[0].length * dim];
        for (int CELL_ROW = 0; CELL_ROW < cells.length; CELL_ROW++) {
            for (int CELL_COL = 0; CELL_COL < cells[0].length; CELL_COL++) {

                byte[][][][] h_images_POS = images.histogramToImages(cells[CELL_ROW][CELL_COL].histogram);
                byte[][][][] h_images_NEG = images.histogramToImages(cells[CELL_ROW][CELL_COL].histogram, -1.0);
                byte[][][] combined_POS = images.add(h_images_POS);
                byte[][][] combined_NEG = images.add(h_images_NEG);
                int[][] int_combined_POS = images.rgbaToINT(combined_POS);
                int[][] int_combined_NEG = images.rgbaToINT(combined_NEG);
                retImagePOS = images.insert(retImagePOS, int_combined_POS, square, CELL_COL * dim, CELL_ROW * dim);
                retImageNEG = images.insert(retImageNEG, int_combined_NEG, square, CELL_COL * dim, CELL_ROW * dim);
            }
        }
        images.saveImageFromIntArray(retImagePOS, res + name + "/" + name + "hogRepPositives.bmp", BufferedImage.TYPE_INT_RGB);
        images.saveImageFromIntArray(retImageNEG, res + name + "/" + name + "hogRepNegatives.bmp", BufferedImage.TYPE_INT_RGB);
        // retImage = insert(retImage, intLine, square, x_ins, y_ins);

    }

    public static void testSubset() {
        Double[][] testArray = new Double[8][8];
        for (int ROW = 0; ROW < testArray.length; ROW++) {
            for (int COL = 0; COL < testArray[0].length; COL++) {
                testArray[ROW][COL] = (double) ROW + COL;
            }
        }
        for (Double[] row : testArray) {
            System.out.println(Arrays.toString(row));
        }

        Double[][] toArray = arrays.subset(testArray, 1, 1, 8, 8, 0.0);
        for (Double[] row : toArray) {
            System.out.println(Arrays.toString(row));
        }
    }

    public static void testGradientSubset() {
        Gradient[][] testArray = new Gradient[100][100];
        for (int ROW = 0; ROW < testArray.length; ROW++) {
            for (int COL = 0; COL < testArray[0].length; COL++) {
                testArray[ROW][COL] = new Gradient(ROW, COL, 170, 2);
            }
        }
        for (Gradient[] row : testArray) {
            System.out.println(Arrays.toString(row));
        }

        Gradient[][] toArray = arrays.subset(testArray, 1, 1, 8, 8, new Gradient(-1, -1, -1, -1));
        for (Gradient[] row : toArray) {
            System.out.println(Arrays.toString(row));
        }
    }

    public static void testGradientHistogram() {

        Gradient[][] cells = new Gradient[8][8];
        for (int ROW = 0; ROW < cells.length; ROW++) {
            for (int COL = 0; COL < cells[0].length; COL++) {
                cells[ROW][COL] = new Gradient(ROW, COL, 170, 2);
            }
        }
        int[] hist = images.applyHistogram(cells);
        System.out.println(Arrays.toString(hist));

    }

    public static void testHistogram() {
        double[][] data = new double[][]{{-0, 0, 20, 40, 60, 80, 100, 120, 140, 160, 180}};
        int[] freq = matrices.frequenciesFromData(data, 0, 180, 9);
        System.out.println(Arrays.toString(freq));
    }

    public static void testGradientSave() throws IOException {
        String name = "bldg";
        byte[][][] original = images.loadImageBytes(res + name + ".bmp");
        byte[][][][] split = images.splitRGB(original);
        int[][][] splitInt = new int[split.length][][];
        for (int i = 0; i < split.length; i++) {
            splitInt[i] = images.rgbaToINT(split[i]);
        }
        strel structure_x = strel.rect(3, 1, 0, 1);
        structure_x.print();
        structure_x.printVec();

        // {{1}, {c}, {1}}
        strel structure_y = strel.rect(1, 3, 1, 0);
        structure_y.print();
        structure_y.printVec();

        double[][] filter_x = {{-1}, {0}, {1}};
        double[][] filter_y = {{-1, 0, 1}};

        images.Gradient[][] slope_field = images.findGradientsMax(splitInt, structure_x, structure_y, filter_x, filter_y, 1.0);
        double[][] thetas = new double[slope_field.length][slope_field[0].length];
        double[][] degrees = new double[slope_field.length][slope_field[0].length];
        for (int ROW = 0; ROW < slope_field.length; ROW++) {
            for (int COL = 0; COL < slope_field[0].length; COL++) {
                thetas[ROW][COL] = slope_field[ROW][COL].positiveTheta();
                degrees[ROW][COL] = thetas[ROW][COL] * 180 / Math.PI;
            }
        }
        String[] thetaString = matrices.toString(thetas);
        files.saveFile(thetaString, res + name + "/" + name + "thetasP.txt");

        String[] degreeString = matrices.toString(degrees);
        files.saveFile(degreeString, res + name + "/" + name + "degreesP.txt");

    }

    public static void testSplit() throws IOException {
        byte[][][] bobImage = images.loadImageBytes(res + "bob.bmp");
        byte[][][][] split = images.splitRGB(bobImage);
        images.saveImageBytes(split[0], res + "bob/bobRed.bmp");
        images.saveImageBytes(split[1], res + "bob/bobGreen.bmp");
        images.saveImageBytes(split[2], res + "bob/bobBlue.bmp");
    }

    public static void testEdgeSplit() throws IOException {
        String name = "bldg";
        byte[][][] original = images.loadImageBytes(res + name + ".bmp");
        byte[][][][] split = images.splitRGB(original);
        for (int i = 0; i < split.length; i++) {
            images.saveImageBytes(split[i], res + name + "/" + name + "Channel" + i + ".bmp");
        }
        int[][][] greyScaled = new int[split.length][][];
        for (int i = 0; i < split.length; i++) {
            greyScaled[i] = images.toGreyscale(split[i]);
        }
        int[][] sobelMax = images.applySobelMax(greyScaled, 1.0);
        images.saveImageFromIntArray(sobelMax, res + name + "/" + name + "SobelMax.bmp", BufferedImage.TYPE_INT_RGB);
        int[][] threshold = images.applyThreshold(images.intToRGB(sobelMax), 0.10);
        images.saveImageFromIntArray(threshold, res + name + "/" + name + "Threshold.bmp", BufferedImage.TYPE_INT_RGB);
    }

    public static void testGif() throws IOException {

        List<byte[][][]> frames = images.loadGif(res + "firework.gif");
        String fileLocation = res + "frames/";
        File dir = new File(fileLocation);
        dir.mkdirs();
        for (int i = 0; i < frames.size(); i++) {
            images.saveImageBytes(frames.get(i), fileLocation + i + ".bmp");
        }
    }

    public static void testEdge() throws IOException {

        String name = "hadi";
        int[][] image = images.loadImageToIntArray(res + name + ".bmp", BufferedImage.TYPE_INT_RGB);
        int[][] greyscale = images.toGreyscale(images.loadImageBytes(res + name + ".bmp"));
        images.saveImageFromIntArray(greyscale, res + name + "/" + name + "Greyscale.bmp", BufferedImage.TYPE_INT_RGB);
        int[][] sobel_applied = images.applySobel(greyscale);
        images.saveImageFromIntArray(sobel_applied, res + name + "/" + name + "Sobel.bmp", BufferedImage.TYPE_INT_RGB);
        byte[][][] sobel_applied_b = images.intToRGB(sobel_applied);
        byte[][][] sobel_threshold = images.applyThreshold_b(sobel_applied_b, 0.5);
        images.saveImageBytes(sobel_threshold, res + name + "/" + name + "Thresh.bmp");
        byte[][][] altered = images.openBinary(sobel_threshold, strel.diamond(3));
        images.saveImageBytes(altered, res + name + "/" + name + "Dilated.bmp");
    }

    public static void testInsert() throws IOException {
        byte[][][] bobImage = images.loadImageBytes(res + "bob.bmp");
        int size = 17;
        byte[][][] redRect = images.solidRect(size, size, new byte[]{(byte) 255, 0, 0});
        strel sq = strel.square(size);
        byte[][][] inserted = images.insert(bobImage, redRect, sq, size / 2, size / 2);
        images.saveImageBytes(inserted, res + "bobInserted.bmp");
    }

    public static void testSloped() throws IOException {

        byte[][][] bldg = images.loadImageBytes(res + "face.bmp");
        int[][] greyscale = images.toGreyscale(bldg);
        int[][] sloped = images.applySlopefield(greyscale, 1.0);
        images.saveImageFromIntArray(sloped, res + "faceSloped.bmp", BufferedImage.TYPE_INT_RGB);
    }

    public static void testMorph(String path, String name) throws IOException {
        byte[][][] original = images.loadImageBytes(path + name + ".bmp");
        String savePath = path + name + "/";
        File file = new File(savePath);
        file.mkdirs();
        String preName = name + "_";
        images.saveImageBytes(original, savePath + "original.bmp");
        byte[][][] greyscale = images.toGreyscale_b(original);
        images.saveImageBytes(greyscale, savePath + "greyscale.bmp");
        byte[][][] threshold = images.applyThreshold_b(greyscale, 0.45);
        images.saveImageBytes(threshold, savePath + "threshold.bmp");
        strel B = strel.diamond(3);
        byte[][][] eroded = images.erodeBinary(threshold, B);
        images.saveImageBytes(eroded, savePath + "eroded.bmp");
        byte[][][] dilated = images.dilateBinary(threshold, B);
        images.saveImageBytes(dilated, savePath + "dilated.bmp");
        byte[][][] opened = images.openBinary(threshold, B);
        images.saveImageBytes(opened, savePath + "opened.bmp");
        byte[][][] closed = images.closeBinary(threshold, B);
        images.saveImageBytes(closed, savePath + "closed.bmp");
    }

}
