package utils;

public interface Initializable {

    public void init();

}
